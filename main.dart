import 'package:flutter/material.dart';
import 'screen/movie_list_view.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Movie App',
      home: MovieListView(),
    );
  }
}
